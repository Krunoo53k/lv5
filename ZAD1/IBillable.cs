﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD1
{
    interface IBillable
    {
        double Price { get; }
        string Description(int depth = 0);
    }
}
