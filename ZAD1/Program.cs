﻿using System;

namespace ZAD1
{
    class Program
    {
        static void Main(string[] args)
        {
            Box boxOfItems = new Box("ShipToAfrica");
            Product penguin = new Product("A suspicious penguin", 1000, 12);
            Product lion = new Product("Likes to eat meat", 1000000, 95);
            boxOfItems.Add(penguin);
            boxOfItems.Add(lion);
            ShippingService OsijekPostalService = new ShippingService(2);
            Console.WriteLine("Cijena dostave lava je: "+OsijekPostalService.CalculatePrice(lion));
            Console.WriteLine("Cijena dostave pingvina je " + OsijekPostalService.CalculatePrice(penguin));
        }
    }
}
