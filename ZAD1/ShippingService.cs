﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD1
{
    class ShippingService
    {
        private double priceToWeightRatio;
        public ShippingService(double priceToWeightRatio)
        {
            this.priceToWeightRatio = priceToWeightRatio;
        }

        public double CalculatePrice(IShipable item)
        {
            return item.Weight * priceToWeightRatio;
        }
    }
}
