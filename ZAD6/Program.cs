﻿using System;

namespace ZAD6
{
    class Program
    {
        static void Main(string[] args)
        {
            HackerTheme hack = new HackerTheme();
            LightTheme light = new LightTheme();    
            ReminderNoteUsernames reminder1 = new ReminderNoteUsernames("Ne zaboravite poslat rppoon", hack);
            reminder1.Add("Kruno");
            reminder1.Add("Pitlisti");
            ReminderNoteUsernames reminder2 = new ReminderNoteUsernames("Ne zaboravi platit rezije", light);
            reminder2.Add("Tonka");
            reminder2.Add("Stipo");
            reminder1.Show();
            reminder2.Show();
        }
    }
}
