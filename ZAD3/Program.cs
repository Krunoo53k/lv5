﻿using System;

namespace ZAD3
{
    class Program
    {
        static void Main(string[] args)
        {
            VirtualProxyDataset dataset = new VirtualProxyDataset("test.txt");
            DataConsolePrinter printer = new DataConsolePrinter(dataset);
            printer.printToConsole();
            User user1=User.GenerateUser("Kruno");
            User user2 = User.GenerateUser("Lopov");
            ProtectionProxyDataset protectionProxyDataset = new ProtectionProxyDataset(user2);
            printer=new DataConsolePrinter(protectionProxyDataset);
            printer.printToConsole();
        }
    }
}
