﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace ZAD4
{
    class DataConsolePrinter
    {
        private List<string> data;
        public DataConsolePrinter(IDataset dataset)
        {
            data = new List<string>();
            if (dataset.GetData() == null)
            {
                Console.WriteLine("Nemate pravo pristupa ovim podatcima.");
            }
            else
            {
                foreach (var element in dataset.GetData())
                {

                    foreach (string str in element)
                    {
                        data.Add(str);
                        data.Add(" ");
                    }
                    data.Add("\n");
                }
            }
        }
        public void printToConsole()
        {
            foreach (string element in data)
                Console.Write(element);
        }
    }
}
