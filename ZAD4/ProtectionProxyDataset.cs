﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace ZAD4
{
    class ProtectionProxyDataset : IDataset
    {
        private Dataset dataset;
        private List<int> allowedIDs;
        private ConsoleLogger logger;
        public User user{private get; set;}

        public ProtectionProxyDataset(User user)
        {
            this.allowedIDs = new List<int>(new int[] { 1, 3, 5 });
            this.user = user;
        }
        private bool AuthenticateUser()
        {
            return allowedIDs.Contains(this.user.ID);
        }
        public ReadOnlyCollection<List<string>>GetData()
        {
            logger = ConsoleLogger.GetInstance();
            logger.Log("Data accesed by " + user.Name + " at " + DateTime.Now);
            if(this.AuthenticateUser())
            {
                if (this.dataset == null)
                {
                    this.dataset = new Dataset("sensitiveData.csv");
                }
                return this.dataset.GetData();
            }
            return null;
        }
    }
}
