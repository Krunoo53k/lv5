﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;

namespace ZAD4
{
    class VirtualProxyDataset : IDataset
    {
        private string filePath;
        private Dataset dataset;
        ConsoleLogger logger;

        public VirtualProxyDataset(string filePath)
        {
            this.filePath = filePath;
        }

        public ReadOnlyCollection<List<string>> GetData()
        {
            logger = ConsoleLogger.GetInstance();
            logger.Log("Data accessed at: " + DateTime.Now);
            if(dataset==null)
            {
                dataset = new Dataset(filePath);
            }
            return dataset.GetData();
        }
    }
}
