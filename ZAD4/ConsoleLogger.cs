﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD4
{
    
        class ConsoleLogger
        {
            private static ConsoleLogger instance;
            string filePath;
            bool appendText;
            private ConsoleLogger()
            {
                this.appendText = false;
                this.filePath = "log.txt";
            }
            public static ConsoleLogger GetInstance()
            {
                if (instance == null)
                    instance = new ConsoleLogger();
                return instance;
            }

            public void Log(string message)
            {

                using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.filePath, appendText))
                {
                    writer.WriteLine(message);
                    appendText = true;
                }
            }
            public void SetFilePath(string filePath)
            {
                this.filePath = filePath;
            }
        }

    
}
