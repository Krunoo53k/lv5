﻿using System;

namespace ZAD4
{
    class Program
    {
        static void Main(string[] args)
        {
            VirtualProxyDataset virtualProxyDataset = new VirtualProxyDataset("test.txt");
            User user = User.GenerateUser("Kruno");
            User user1 = User.GenerateUser("Smotljo smotani");
            ProtectionProxyDataset protectionProxyDataset = new ProtectionProxyDataset(user);
            DataConsolePrinter printer = new DataConsolePrinter(protectionProxyDataset);
            printer.printToConsole();
            protectionProxyDataset = new ProtectionProxyDataset(user1);
            printer = new DataConsolePrinter(protectionProxyDataset);
            printer.printToConsole();

        }
    }
}
