﻿using System;

namespace ZAD5
{
    class Program
    {
        static void Main(string[] args)
        {
            HackerTheme hack = new HackerTheme();
            ReminderNote reminderNote = new ReminderNote("Ne zaboravi hakirati NASA-u!!", hack);
            reminderNote.Show();
        }
    }
}
