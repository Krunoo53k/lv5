﻿using System;

namespace ZAD7
{
    class Program
    {
        static void Main(string[] args)
        {
            HackerTheme hack = new HackerTheme();
            LightTheme light = new LightTheme();    
            ReminderNoteUsernames reminder1 = new ReminderNoteUsernames("Ne zaboravite poslat rppoon", hack);
            reminder1.Add("Kruno");
            reminder1.Add("Pitlisti");
            ReminderNoteUsernames reminder2 = new ReminderNoteUsernames("Ne zaboravi platit rezije", light);
            reminder2.Add("Tonka");
            reminder2.Add("Stipo");
            Notebook notebook = new Notebook();
            Notebook notebook1 = new Notebook(light);
            notebook.AddNote(reminder1);
            notebook.AddNote(reminder2);
            notebook.Display();
            notebook1.AddNote(reminder1);
            notebook1.AddNote(reminder2);
            notebook1.Display();
            notebook.ChangeTheme(hack);
            notebook.Display();
        }
    }
}
