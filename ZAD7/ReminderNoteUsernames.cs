﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ZAD7
{
    class ReminderNoteUsernames:Note
    {
        List<string> names = new List<string>();
        public ReminderNoteUsernames(string message, ITheme theme) : base(message, theme) { }

        public override void Show()
        {
            this.ChangeColor();
            Console.WriteLine("REMINDER FOR: ");
            foreach(string name in names)
            {
                if (name.Equals(names.Last()))
                    Console.Write(name + ".");
                else
                    Console.Write(name + ", ");
            }
            Console.WriteLine();
            string framedMessage = this.GetFramedMessage();

            Console.WriteLine(framedMessage);
            Console.ResetColor();
        }

        public void Add(string name)
        {
            names.Add(name);
        }
        public void Remove(string name)
        {
            names.Remove(name);
        }

    }
}
