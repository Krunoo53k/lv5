﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD7
{
    class Notebook
    {
        private List<Note> notes;
        ITheme theme;
        bool themeSet;
        public Notebook() { this.notes = new List<Note>();themeSet = false; }
        public Notebook(ITheme theme)
        {
            this.notes = new List<Note>();
            this.theme = theme;
            this.themeSet = true;
        }

        public void AddNote(Note note)
        {
            if (themeSet)
            {
                note.Theme = this.theme;
                notes.Add(note);
            }
            else
                notes.Add(note);
        }
        public void ChangeTheme(ITheme theme)
        {
            this.theme = theme;
            this.themeSet = true;
            foreach(Note note in this.notes)
            {
                note.Theme = theme;
            }
        }
        
        public void Display()
        {
            foreach (Note note in this.notes)
            {
                note.Show();
                Console.WriteLine("\n");
            }


        }
    }
}
